// const chai = require('chai')                                // require Chai framework
// const chaiHttp = require('chai-http')                       // Require chai http framework
// const database = require('../src/database')                 // Require database
// const assert = require('assert')                            // Require assert library

// chai.should()                                               // Using chai Should
// chai.use(chaiHttp)                                          // Include ChaiHttp for later online tests

// // Test van MovieDatabase
// describe("MovieDatabase", () =>
// {
//     // Wat verwacht je van de test?
//     it("Should save a movie in the database", (done) =>
//     {
//         // zet een movie in de database
//         const movie = {
//             "title" : "New Kids: Turbo",
//             "description" : "Noord-Brabant is best brabant",
//             "releaseYear" : 2014,
//             "director" : "Barry Badpak"
//         }
//         database.movies.push(movie)

//         // vraag correcte movie positie op in de array, of te wel, de laatste.
//         const pos = database.movies.length -1

//         // verachting: Als het gelukt is: db[0] == movie
//         assert(database.movies[pos].title === movie.title)
//         assert(database.movies[pos].description === movie.description)
//         assert(database.movies[pos].releaseYear === movie.releaseYear)
//         assert(database.movies[pos].director === movie.director)
//         done()
//     })
// })
