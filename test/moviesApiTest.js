// const chai = require('chai')                // require Chai framework
// const chaiHttp = require('chai-http')       // Require chai http framework
// const server = require('../index')          // Require server index

// chai.should()                               // Using chai Should
// chai.use(chaiHttp)                          // Include ChaiHttp for later online tests

// const endPointToTest = '/api/movies/'

// // Test van Movie POST
// describe('/POST Movie', () =>
// {
//     // verwacht dat bij POST movie dat er een movieID word terugegeven
//     it('api/movies posts movie and returns movieID', (done) =>
//     {
//         // Maak een POST request with een film
//         chai.request(server)
//             .post(endPointToTest)
//             .set('logintoken', '5555')
//             .set('Content-Type', 'application/json')
//             .send({
//                 "title": "New Kids: Nitro",
//                 "description": "Nog brabander dan turbo",
//                 "releaseYear": 2016,
//                 "director": "Barry Badpak"
//             })
//             .end((err, res) =>
//             {
//                 res.should.have.status(200)                                                      // Controleer of je satus 200 returned
//                 res.body.should.have.property('title').that.is.a('string')                       // controleer of title wel een string is
//                 res.body.should.have.property('description').equals('Nog brabander dan turbo')   // Controleer of de beschrijving gelijk is aan de beschrijving van de post
//                 res.body.should.have.property('releaseYear').equals(2016)                        // Controleer of de releaseYear het zelfde is
//                 res.body.should.have.property('releaseYear').that.is.a('number')                 // Controleer of releaseYear wel een cijfer is
//                 res.body.should.have.property('director').equals('Barry Badpak')                 // Controleer of de director het zelfde is
//                 res.body.should.have.property('movieID').that.is.a('number')                     // Controleer of er een movieID word gegeven dat een getal is
//                 res.body.should.not.have.property('password')                                    // Controleer of er geen password word gegeven
//                 done()
//             })
//     })
// })

// //Testing movie GET request
// describe('/GET/:ID Movie', () =>
// {
//     const movieID = 0;  // the movie we want to find.

//     // zoek film op ID en verwacht film object terug
//     it('api/movies/0 returns movie object', (done) =>
//     {
//         // Maak een GET request met movieID 0
//         chai.request(server)
//             .get(endPointToTest + movieID)
//             .set('logintoken', '5555')
//             .end((err, res) =>
//             {
//                 // Controleer of je statuscode 200 en een object terug krijgt
//                 res.should.have.status(200)

//                 // Controleer of het object de movie attributen bevat
//                 res.body.should.have.property('title').equals('New Kids: Nitro')
//                 res.body.should.have.property('description').equals('Nog brabander dan turbo')
//                 res.body.should.have.property('releaseYear').equals(2016)
//                 res.body.should.have.property('director').equals('Barry Badpak')
//                 done()
//             })
//     })
// })

// //Testing movie PUT request
// describe('/PUT/:ID Movie', () =>
// {
//     const movieID = 0;  // the movie we want to find.

//     // zoek film op ID en verwacht film object terug
//     it('api/movies/0 updates movie in giving position', (done) =>
//     {
//         // Maak een GET request met movieID 0
//         chai.request(server)
//             .put(endPointToTest + movieID)
//             .set('logintoken', '5555')
//             .set('Content-Type', 'application/json')
//             .send({
//                 "title": "Jackass: The Movie",
//                 "description": "Helemaal gek",
//                 "releaseYear": 2009,
//                 "director": "Jeff Tremaine"
//             })
//             .end((err, res) =>
//             {
//                 // Controleer of je statuscode 200 en een object terug krijgt
//                 res.should.have.status(200)

//                 // Controleer of het object de movie attributen bevat
//                 res.body.should.have.property('title').equals('Jackass: The Movie')
//                 res.body.should.have.property('description').equals('Helemaal gek')
//                 res.body.should.have.property('releaseYear').equals(2009)
//                 res.body.should.have.property('director').equals('Jeff Tremaine')
//                 done()
//             })
//     })
// })

// //Testing movie PUT request
// describe('/DELETE/:ID Movie', () =>
// {
//     const movieID = 0; // the movie we want to find.

//     // zoek film op ID en verwacht film object terug
//     it('api/movies/0 deletes movie in giving position', (done) =>
//     {
//         // Maak een GET request met movieID 0
//         chai.request(server)
//             .delete(endPointToTest + movieID)
//             .set('logintoken', '5555')
//             .end((err, res) =>
//             {
//                 // Controleer of je statuscode 200 en een object terug krijgt
//                 res.should.have.status(200)

//                 console.log(res.body)
//                 done()
//             })
//     })
// })
