// const chai = require('chai')                                // require Chai framework
// const chaiHttp = require('chai-http')                       // Require chai http framework
// const database = require('../src/database')                 // Require database
// const assert = require('assert')                            // Require assert library

// chai.should()                                               // Using chai Should
// chai.use(chaiHttp)                                          // Include ChaiHttp for later online tests

// // Test van UserDatabase
// describe("RegisterDatabase", () =>
// {
//     // Wat verwacht je van de test?
//     it("Should save a user in the database", (done) =>
//     {
//         // Create user from data collected from the POST
//         const user =
//         {
//             name: "Stefan Ilmer",
//             adress: "Den Uylhof 27",
//             zipcode: "4871 GV",
//             city: "Etten-Leur",
//             dateOfBirth: "05-09-1992",
//             phoneNumber: "0657184940",
//             email: "sr.ilmer@student.avans.nl",
//             password: "brabantIsBeterDanRotterdam"
//         };

//         database.users.push(user)

//         // vraag correcte movie positie op in de array, of te wel, de laatste.
//         const pos = database.users.length -1

//         // verachting: Als het gelukt is: db[0] == user
//         assert(database.users[pos].name === user.name)
//         assert(database.users[pos].adress === user.adress)
//         assert(database.users[pos].zipcode === user.zipcode)
//         assert(database.users[pos].city === user.city)
//         assert(database.users[pos].dateOfBirth === user.dateOfBirth)
//         assert(database.users[pos].phoneNumber === user.phoneNumber)
//         assert(database.users[pos].email === user.email)
//         assert(database.users[pos].password === user.password)
//         done()
//     })
// })
