const express = require('express')                  // Include Express files
const bodyParser = require('body-parser')           // Include body parser
const logger = require('./src/config/appconfig')    // Include app config file

// Create routers
const routeMovies   = require('./src/routes/movies')
const routeRegister = require('./src/routes/register')

const app = express()
const port = process.env.PORT || 3000

// Lambda function: () => {}
let myCallBackFunction = function() 
{
    console.log('De server luistert op port ' + port);
}

app.listen(port, myCallBackFunction);
app.use(express.json())
app.use(bodyParser.json())

// set routers
// NOOIT GELIJKE NAMEN GEVEN
app.use('/api/movies', routeMovies)
app.use('/api/register', routeRegister)

// Export server voor toegang in andere files
module.exports = app