// GET method route
app.get('/', function (req, res) {
    res.send('root')
  })
  
  // GET method route url/about
  app.get('/about', function (req, res) 
  {
    res.send('about')
  })
  
  // GET method route url/random.text
  app.get('/random.text', function (req, res) 
  {
    res.send('random.text')
  })
  
  // GET method route based on string paterns
  // Route path will match 'acd' and 'abcd'
  app.get('/ab?cd', function (req, res)
  {
    res.send('ab?cd')
  })
  
  // GET method route based on string paterns
  // Route will match 'abcd', 'abbcd', 'abbbbbbbbbbbbbbbbcd' and on
  app.get('/ab+cd', function (req, res) 
  {
    res.send('ab+cd')
  })
  
  // Get method route based on string paterns
  // Route will match 'abcd', 'abINSERTRANDOMTEXTHEREcd', 'ab1234cd' and on
  app.get('/ab*cd', function (req, res) 
  {
    res.send('ab*cd')
  })
  
  // GET method route based on string paterns
  // Route will match '/abe', '/abcde'
  app.get('/ab(cd)?e', function (req, res)
  {
    res.send('ab(cd)?e')
  })
  
  // GET method route based on regular expressions
  // Route will match with anything that has an 'a' in it
  app.get(/a/, function (req, res) {
    res.send('/a/')
  })
  
  // GET method route based on regular expressions
  // Route will match with anything that END with fly
  app.get(/.*fly$/, function (req, res) 
  {
    res.send('/.*fly$/')
  })


  // callback using an array of functions
  var cb0 = function (req, res, next) {
    console.log('CB0')
    next()
  }
  
  var cb1 = function (req, res, next) {
    console.log('CB1')
    next()
  }
  
  var cb2 = function (req, res) {
    res.send('Hello from C!')
  }
 
  // ALWAYS PUT THIS BELOW FUNCTIONS
  // Calls functions listed
  app.get('/example/c', [cb0, cb1, cb2])


  // callback using an array of functions
  var cb0 = function (req, res, next) {
    console.log('CB0')
    next()
  }
  
  var cb1 = function (req, res, next) {
    console.log('CB1')
    next()
  }
  
  // ALWAYS PUT THIS BELOW THE FUNCTIONS
  // will call the given functions and normal callback.
  app.get('/example/d', [cb0, cb1], function (req, res, next) 
  {
    console.log('the response will be sent by the next function ...')
    next()
  }, function (req, res) 
  {
    res.send('Hello from D!')
  })


  // Tracer logging

// Include Trace logging, Gebruiker er 1.
// normale console(); geeft alles in zwart/wit
// colorconsole geeft elke niveau een andere kleur
// colorconsole met level, geeft alleen bepaalde niveau's weer.

//var logger = require('tracer').console();
//var logger = require('tracer').colorConsole();
//var logger = require('tracer').colorConsole({level:'warn'});


// Logging directory and file name
//console.log(__dirname);
//console.log(__filename);