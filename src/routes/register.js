const express = require('express')
const router = express.Router()
const registerController = require('../controllers/registerController')
const authController = require('../controllers/authController')

router.all("*", registerController.RegisterHandler)

// POST - voegt een user toe - returned userID
router.post('/', authController.validate, registerController.RegisterUser)

// GET - Geeft de user met het gegeven userID - returned user informatie
router.get('/:userId', authController.validate, registerController.GetUser)

router.all("*", registerController.RegisterEndPointhandler)

router.use(registerController.RegisterErrorHandler)

module.exports = router