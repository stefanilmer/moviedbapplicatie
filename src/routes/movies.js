const express = require('express')
const router = express.Router()
const movieController = require('../controllers/movieController')
const authController = require('../controllers/authController')

router.all("*", movieController.MovieHandler)

// POST - voegt een movie toe - returned movieID
router.post('/', authController.validate, movieController.CreateMovie)

// GET - Geeft alle movies
router.get('/', authController.validate, movieController.GetAllMovies)

// GET - Geeft de movie met het gegeven positie - returned movie informatie
router.get('/:movieId', authController.validate, movieController.GetMovie)

// PUT - Update oude movie met nieuwe  
router.put('/:movieId', authController.validate, movieController.UpdateMovie)

// DELETE - Delete movie op aangegeven plaats
router.delete('/:movieId', authController.validate, movieController.DeleteMovie)

router.all("*", movieController.movieEndpointHandler)

router.use(movieController.MovieErrorHandler)

module.exports = router