const sql = require("mssql"); // require mssql om connectie naar de DB te maken
const config = require("../config/appconfig"); // require je config met DB info
const logger = config.logger;
const dbconfig = config.dbConfig;

module.exports = {
  executeQuery: (query, callback) => {
    sql.connect(dbconfig, err => {
      // ... error checks
      if (err) {
        logger.error("Error connecting: ", err.toString());
        callback(err, null);
        sql.close();
      }
      if (!err) {
        // Query
        new sql.Request().query(query, (err, result) => {
          // ... error checks
          if (err) {
            logger.error("error", err.toString());
            callback(err, null); // result is null omdat je een error hebt
            sql.close();
          }
          if (result) {
            logger.info(result);
            callback(null, result); // je hebt een result dus error mag null
            sql.close();
          }
        });
      }
    });
  }
};
