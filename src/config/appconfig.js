module.exports = {
  logger: require("tracer").colorConsole({
    format: [
      "{{timestamp}} <{{title}}> {{message}} (in {{file}}:{{line}})", //default format
      {
        error: "{{timestamp}} <{{title}}> {{message}} (in {{file}}:{{line}})" // error format
      }
    ],
    dateformat: "HH:MM:ss.L",
    preprocess: function(data) {
      data.title = data.title.toUpperCase();
    },
    level: process.env.LOG_LEVEL || "trace" // Aanpassen vanaf welk level je de trace logs ziet
  }),

  // Config database Erik
  // username:        progr4
  // password:        password123
  // hostname:        aei-sql.avans.nl
  // databasenaam:    MovieAppProgrammeren4
  // Port:            1443

  // Eigen database configuratie
  // username:        nodeJSDBmovie
  // password:        test
  // hostname:        localhost
  // databasenaam:    MovieAppProgrammeren4

  dbConfig: {
    user: process.env.DB_USERNAME || "nodeJSDBmovie",
    password: process.env.DB_PASSWORD || "test",
    server: process.env.DB_HOSTNAME || "localhost",
    database: process.env.DB_DATABASENAME || "MovieAppProgrammeren4",
    port: 1433,
    driver: "msnodesql",
    connectionTimeout: 1500,
    options: {
      // 'true' if you're on Windows Azure
      encrypt: false
    }
  }
};
