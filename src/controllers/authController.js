const logger = require('../config/appconfig').logger

module.exports = 
{
    validate: function(req, res, next)
    {
        const loginToken = req.headers['logintoken']
        
        if(!loginToken || loginToken === null || loginToken == 0)
        {
            // Geen toegang
            const errorObject = 
            {
                message: "Incorrect logintoken",
                code: 403
              }
              logger.error(errorObject)
              next(errorObject)
        }
        else
        {
            next()
        }  
    }
}