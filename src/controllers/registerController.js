const logger = require("../config/appconfig").logger;
const database = require("../repository/mssql.dao");

// TODO
// Goede return values
// Prepared statements

module.exports = {
  RegisterHandler: function(req, res, next) {
    logger.trace("Register handler");
    next();
  },

  RegisterUser: function(req, res, next) {
    logger.info("api/register/");

    // vang user infor op vanuit de body
    const user = req.body;

    // Maak query, Gebruik [User] omdat user een keyword is voor SQL.
    const query =
      "INSERT INTO [User] (Firstname, Lastname, StreetAddress, PostalCode, City, DateOfBirth, PhoneNumber, EmailAddress, Password) OUTPUT INSERTED.UserId VALUES ('" +
      user.firstname +
      "', '" +
      user.lastname +
      "', '" +
      user.streetAddress +
      "', '" +
      user.postalCode +
      "', '" +
      user.city +
      "', '" +
      user.dateOfBirth +
      "', '" +
      user.phoneNumber +
      "', '" +
      user.emailAddress +
      "', '" +
      user.password +
      "')";

    logger.trace(query);

    // query uitvoeren
    database.executeQuery(query, (err, rows) => {
      // Verwerk error of result
      if (err) {
        const errorObject = {
          message: "Er ging iets mis in de database",
          code: 500
        };

        logger.error(errorObject);
        next(errorObject);
      }

      if (rows) {
        res.status(200).json({ result: rows.recordsets });
      }
    });
  },

  GetUser: function(req, res, next) {
    logger.info("api/register/");

    // vang userID op uit de parameters
    const userID = req.params.userId;

    // Maak query
    const query = "SELECT * FROM [User] WHERE [User].UserId = '" + userID + "'";
    logger.trace(query);

    // query uitvoeren
    database.executeQuery(query, (err, rows) => {
      // Verwerk error of result
      if (err) {
        const errorObject = {
          message: "Er ging iets mis in de database",
          code: 500
        };

        logger.error(errorObject);
        next(errorObject);
      }

      if (rows) {
        res.status(200).json({ result: rows.recordsets });
      }
    });
  },

  RegisterEndPointhandler: function(req, res, next) {
    const errorObject = {
      message: "Endpoint not found",
      code: 404
    };
    logger.error(errorObject);
    next(errorObject);
  },

  RegisterErrorHandler: function(error, req, res, next) {
    logger.error("Error Handler aangeroepen", error.message);
    res.status(500).json(error);
  }
};
