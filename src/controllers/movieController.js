const logger = require("../config/appconfig").logger;
const database = require("../repository/mssql.dao");

// TODO
// Prepared statements

module.exports = {
  MovieHandler: function(req, res, next) {
    logger.trace("movie handler");
    next();
  },

  CreateMovie: function(req, res, next) {
    logger.info("api/movies/");

    // vang movie op vanuit body
    const movie = req.body;

    // Maak query
    const query =
      "INSERT INTO Movie (Title, Description, Year) OUTPUT INSERTED.MovieId VALUES ('" +
      movie.title +
      "', '" +
      movie.description +
      "', '" +
      movie.releaseYear +
      "')";

    logger.trace(query);

    // query uitvoeren
    database.executeQuery(query, (err, rows) => {
      // Verwerk error of result
      if (err) {
        const errorObject = {
          message: "Er ging iets mis in de database",
          code: 500
        };

        logger.error(errorObject);
        next(errorObject);
      }

      if (rows) {
        res.status(200).json({ result: rows.recordsets });
      }
    });
  },

  GetAllMovies: function(req, res, next) {
    // Maak query
    const query = "SELECT * FROM Movie";
    logger.trace(query);

    // query uitvoeren
    database.executeQuery(query, (err, rows) => {
      // Verwerk error of result
      if (err) {
        const errorObject = {
          message: "Er ging iets mis in de database",
          code: 500
        };

        logger.error(errorObject);
        next(errorObject);
      }

      if (rows) {
        res.status(200).json({ result: rows.recordsets });
      }
    });
  },

  GetMovie: function(req, res, next) {
    // vang movieID op uit de parameters
    const movieID = req.params.movieId;

    // Maak query
    const query = "SELECT * FROM Movie WHERE Movie.MovieId = " + movieID;
    logger.trace(query);

    // query uitvoeren
    database.executeQuery(query, (err, rows) => {
      // Verwerk error of result
      if (err) {
        const errorObject = {
          message: "Er ging iets mis in de database",
          code: 500
        };

        logger.error(errorObject);
        next(errorObject);
      }

      if (rows) {
        res.status(200).json({ result: rows.recordsets });
      }
    });
  },

  UpdateMovie: function(req, res, next) {
    logger.info("api/movies/");

    // vang movieID op uit de parameters
    const movieID = req.params.movieId;

    // vang movie op vanuit body
    const movie = req.body;

    // Maak query
    const query =
      "UPDATE Movie SET Title = '" +
      movie.title +
      "', Description = '" +
      movie.description +
      "', Year = '" +
      movie.releaseYear +
      "' WHERE MovieId = '" +
      movieID +
      "'";
    logger.trace(query);

    // query uitvoeren
    database.executeQuery(query, (err, rows) => {
      // Verwerk error of result
      if (err) {
        const errorObject = {
          message: "Er ging iets mis in de database",
          code: 500
        };

        logger.error(errorObject);
        next(errorObject);
      }

      if (rows) {
        res.status(200).json(); // Geen output volgens de opdrachtbeschrijving
      }
    });
  },

  DeleteMovie: function(req, res, next) {
    // vang movieID op uit de parameters
    const movieID = req.params.movieId;

    // Maak query
    const query = "DELETE FROM Movie WHERE MovieId = '" + movieID + "'";

    // query uitvoeren
    database.executeQuery(query, (err, rows) => {
      // Verwerk error of result
      if (err) {
        const errorObject = {
          message: "Er ging iets mis in de database",
          code: 500
        };

        logger.error(errorObject);
        next(errorObject);
      }

      if (rows) {
        res.status(200).json(); // Geen output volgens de opdrachtbeschrijving
      }
    });
  },

  movieEndpointHandler: function(req, res, next) {
    const errorObject = {
      message: "Movie Endpoint not found",
      code: 404
    };
    logger.error(errorObject);
    next(errorObject);
  },

  MovieErrorHandler: function(error, req, res, next) {
    logger.error("Movie Error Handler aangeroepen", error.message);
    res.status(500).json(error);
  }
};
